<?php
  $servername = "localhost";
  $dbname = "gogobus";
  $username = "root";
  $password = "1234";
  //echo "test";
  try {
    $conn = new PDO(
      "mysql:host=$servername;dbname=$dbname;",
      $username, $password
    );
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  }
  catch(PDOException $e){
    echo "Connection failed: " . $e->getMessage();
  }
?>